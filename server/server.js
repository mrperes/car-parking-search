"use strict";
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
// define our app using express
var app = express();
//Db configuration
const config_1 = require('./config/config');
//cors to enable receive request from different domains 
app.use(cors());
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// connect to our database
mongoose.connect(config_1.config.db);
var db = mongoose.connection;
db.on('error', () => {
    console.log('unable to connect to database at ' + config_1.config.db);
});
// ROUTES FOR OUR API
// =============================================================================
// get an instance of the express Router
var router = express.Router();
//Map our routes 
const routes_1 = require('./api/routes');
routes_1.expressRoutes(router);
// Register our routes. 
// All of our routes will be prefixed with /api
app.use('/api', router);
// START THE SERVER
// =============================================================================
app.listen(config_1.config.port);
console.log('Magic happens on port ' + config_1.config.port);
// ******** ISSO DEVE SER DIVIDIDO NAS PASTAS ****
// router.route('/bears')
//     // create a bear (accessed at POST http://localhost:8080/api/bears)
//     .post(function(req, res) {
//         var car = new Car();      // create a new instance of the Bear model
//         car.plaque = req.body.plaque;  // set the bears name (comes from the request)
//         // save the bear and check for errors
//         car.save(function(err) {
//             if (err)
//                 res.send(err);
//             res.json({ message: 'Bear created!' });
//         });
//     })
//     // get all the bears (accessed at GET http://localhost:8080/api/bears)
//     .get(function(req, res) {
//         console.log("chegou");
//         Car.find(function(err, cars) {
//             console.log(err);
//             if (err)
//                 res.send(err);
//             res.json(cars);
//             console.log(res);
//         });
//     }); 
//# sourceMappingURL=server.js.map