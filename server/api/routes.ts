//Controllers
import { carController } from './controllers/car';
import { userController } from './controllers/user';
import { notifyController } from './controllers/notify';

//Services
import { authenticationService } from './services/authentication';

export const expressRoutes = (router: any) => {

    // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
    router.get('/test', function (req, res) {
        res.json({ message: 'hooray! welcome to our api without authentication! ' });
    });


    var validate = authenticationService.middlewareValidadeToken;
    //var validate = (req, res, next) => { next(); };

    //User controller
    router.post("/login", userController.login);
    router.post("/register", validate, userController.register);  
    router.get("/getUserInfo", validate, userController.getUserInfo);
    router.get("/redirect", validate, userController.redirect);
    router.post("/saveUser", validate, userController.save); 
    //Car controller
    router.get("/getAllCars", validate, carController.getAllCars);
    router.post("/createSimpleCar", validate, carController.createSimpleCar);
    //Notify
    router.get("/getAllSent", validate, notifyController.getAllNotificationUserSent);
    router.get("/getAllReceived", validate, notifyController.getAllNotificationUserReceived); 
    router.post("/notifyBlocking", validate, notifyController.sendNotification);
    router.post("/notifyLight", validate, notifyController.sendNotification);

}