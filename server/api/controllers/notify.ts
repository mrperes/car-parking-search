import { notifyService } from '../services/notify';

import { INotify } from '../models/notify';

export class NotifyController {

    sendNotification(req, res) {
        let notify = {
            Plaque: req.body.plaque,
            UserSender: req.userId, 
            UserReceiver: ''
        };
        notifyService.sendNotification(notify)
            .then((notifications: [INotify]) => {
                res.json({
                    message: "Notification sent to " + notifications.map(p => p.plaque).join(', ')
                })
            }, (err) => {
                res.json({
                    message: err.message 
                })
            })
    }

    getAllNotificationUserSent(req, res) {
        notifyService.findAllSentByUser(req.userId)
            .then((notifications) => {
                res.json(notifications); 
            })
    }

    getAllNotificationUserReceived(req, res) {
        
    }
}

export const notifyController = new NotifyController();