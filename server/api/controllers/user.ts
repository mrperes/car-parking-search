import { authenticationService } from '../services/authentication';
import { userService } from '../services/user';

export class UserController {

    login(req, res) {
        switch (req.body.loginApi) {
            case 'google': {
                authenticationService.authenticateGoogle(req)
                    .then((result: any) => {
                        if (result) {
                            res.json({
                                access_token: result.token,
                                userId: result.userId
                            });
                        }
                        else {
                            res.json({
                                statusCode: 403
                            });
                        }
                    });
            }
            default: {

            }
        }
    }

    getUserInfo(req, res) { 
        userService.findById(req.userId)
            .then((user) =>
                res.json({
                    email: user.google.email,
                    name: user.name,
                    plaques: user.plaques
                }),
                (err) => { throw err });
    }

    redirect(req, res) {
        res.json({
            statusCode: 300 
        })
    }

    save(req, res) { 
        userService.save(req.userId, req.body)
            .then((user) => {
                res.json({
                    email: user.google.email,
                    name: user.name,
                    plaques: user.plaques
                });
            });
    }

    register(req, res) {
        userService.register(req.userId, req.body)
            .then((user) => {
                res.json({
                    msg: 'Salvou'
                });
            });
    }

}

export const userController = new UserController();