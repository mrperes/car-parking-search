import { carService } from '../services/car';

export class CarController {

    getAllCars(req, res) {        
        carService.findAll()
        .then((cars) => {
            res.json({cars});
        });
    }

    createSimpleCar(req, res) {
        console.log(req.body.plaque); 
        carService.createMockCar(req.body.plaque)
        .then(() => {
            res.json({mensagem: "wooow"});
        });
    }

}

export const carController = new CarController();