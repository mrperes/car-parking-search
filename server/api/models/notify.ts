import mongoose = require('mongoose');
var Schema = mongoose.Schema;

export interface INotify extends mongoose.Document {
    id: string,
    userSender: string,
    userReceiver: string,
    plaque: string
}

const NotifySchema = new mongoose.Schema({
    userSender: String,
    userReceiver: String,
    plaque: String
});

export const Notify = mongoose.model<INotify>('Notify', NotifySchema); 