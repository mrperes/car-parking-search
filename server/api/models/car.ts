import mongoose = require('mongoose');
var Schema = mongoose.Schema;

export interface ICar extends mongoose.Document {
    plaque: string
}

const CarSchema = new Schema({
    plaque: String
});

export const Car = mongoose.model<ICar>('Car', CarSchema); 