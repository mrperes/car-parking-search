import mongoose = require('mongoose');

var Schema = mongoose.Schema;

export interface IUser extends mongoose.Document {
    id: string,
    name: string,
    plaques: string[],
    google: {
        id: string,
        email: string,
        token: string,
        firstName: string,
        familyName: string
    },
    devices: string[]
}

const UserSchema = new mongoose.Schema({
    name: String,
    plaques: [String],
    google: {
        id: String,
        email: String,
        token: String,
        firstName: String,
        familyName: String
    },
    devices: [String]
});

export const User = mongoose.model<IUser>('User', UserSchema); 