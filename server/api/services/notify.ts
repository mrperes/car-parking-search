import q = require('q');
import gcm = require('node-gcm');

import { config } from '../../config/config'; 

import { userService } from './user';

import { INotify, Notify } from '../models/notify';
import { IUser } from '../models/User';

class NotifyService {

    findAllSentByUser(userId: string): q.IPromise<INotify> {
        var deferred = q.defer();

        Notify.find({ 'userSender': userId },
            function (err, cars) {
                if (err) {
                    console.log(err);
                }
                else {
                    deferred.resolve(cars);
                }
            });

        return deferred.promise;
    }

    sendNotification(data: any): q.IPromise<[INotify]> {
        var deferred = q.defer<[INotify]>();

        userService.findByPlaque(data.Plaque)
            .then((users) => {
                //Se nao existir usuário com esta placa.
                if (!users) deferred.reject("Nenhum usuário cadastrado com esta placa");


                let notifies = [];
                let promises = users
                    .map(user =>
                        new Promise((resolve) => {
                            let nNotification = new Notify();
                            nNotification.userSender = data.UserSender;
                            nNotification.plaque = data.Plaque;
                            nNotification.userReceiver = user.id;

                            nNotification.save((err, saved) => {
                                if (err) {
                                    deferred.reject(err);
                                }
                                else {
                                    notifies.push(saved);
                                    this.sendNotificationToUser(user);
                                    resolve(saved);
                                }
                            });
                        })
                    );

                Promise.all(promises).then((p) => {
                    deferred.resolve(<[INotify]>notifies)
                });

            })

        return deferred.promise;
    }

    sendNotificationToUser(user: IUser) {
        console.log('sending to' + user.name);

        // Set up the sender with your GCM/FCM API key (declare this once for multiple messages)
        //https://console.firebase.google.com/project/car-parking-158120/settings/cloudmessaging -> Chave do servidor
        let clientId = config.google.clientId;
        let sender = new gcm.Sender('AAAAXDXH8Vo:APA91bHFlgQX5eW00Z2DoFligZJI0kY59atRNyajiiF77pefSoqxZJI-k5kYgJDt0wUi0O9jhok4XvF8CIZWGAPSKIHAuBiLYLLnP0K8g7-zhkVs6Nz9JijwO5o3AdTZE93sFERKFYAb');
 
        // Prepare a message to be sent
        let message = new gcm.Message({
            data: { key1: 'msg1' }
        });

        // Specify which registration IDs to deliver the message to
        let regTokens = user.devices; 

        // Actually send the message
        sender.send(message, { registrationTokens: regTokens }, function (err, response) {
            if (err) console.error(err);
            else console.log(response);
        });
    }
}

export const notifyService = new NotifyService()