import q = require('q');

import { IUser, User } from '../models/user';

class UserService {

    findById(id: string): q.IPromise<IUser> {
        var deferred = q.defer();

        User.findById(id, function(err, user) {
            if(err) {
                console.log(err);
            }
            else {
                deferred.resolve(user);
            }
        });

        return deferred.promise;
    }

    findByPlaque(plaque: string): q.IPromise<[IUser]>{
        var deferred = q.defer<[IUser]>();
        User.find({ 'plaques': { '$in': [plaque] } }, 
            (err, user: [IUser]) => {
                if(err) {
                    deferred.reject(err);
                }
                else {
                    deferred.resolve(user);
                }
            });
        return deferred.promise;
    }

    save(userId: number, data: any): q.IPromise<IUser> { 
        var deferred = q.defer<IUser>();

        User.findByIdAndUpdate(userId, {},
            (err, user) => { 
                if(err) throw err;
                //if(err) throw err;

                user.name = data.name;
                user.plaques = data.plaques;

                user.save(function (err, updatedUser) {
                    if(err){
                        deferred.reject(err);
                    }
                    else {
                        deferred.resolve(updatedUser);
                    }
                });

            }); 

        return deferred.promise; 
    }

    register(userId: number, data: any): q.IPromise<string> {
        var deferred = q.defer<string>();

        User.findByIdAndUpdate(userId, {},
            (err, user) => { 
                if(err) throw err;
                //if(err) throw err;

                if(!user.devices){
                    user.devices = [];
                } 

                user.devices.push(data.deviceId);

                user.save(function (err, updatedUser) {
                    if(err){
                        deferred.reject(err);
                    }
                    else {
                        deferred.resolve('registrou no back o deviceId');
                    }
                });

            }); 

        return deferred.promise; 
    }
}

export const userService = new UserService()