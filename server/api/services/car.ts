import q = require('q');

import {ICar, Car} from '../models/car';

class CarService {

    findAll(): q.IPromise<ICar>{
        var deferred = q.defer();
        
        Car.find({}, function(err, cars) {
            if(err) {
                console.log(err);
            }
            else {
                deferred.resolve(cars);
            }
        });

        return deferred.promise;
    }

    createMockCar(plaque: string){
        var deferred = q.defer();
        
        var car = new Car({ 
            plaque: plaque
        });

        // save the sample user
        car.save(function(err, car) {
            if (err) throw err;
            
            deferred.resolve(car);
        });

        return deferred.promise;
    }


}

export const carService = new CarService()