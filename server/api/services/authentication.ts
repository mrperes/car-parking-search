import q = require('q');
import jwt = require('jsonwebtoken');

import { config } from '../../config/config';

import {IUser, User} from '../models/user';

import https = require('https');

class AuthenticationService {

    middlewareValidadeToken(req, res, next) { 

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, config.jsonWebTokenSecret, function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;

                    req.userId = req.headers['user-id'];

                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }

    };

    authenticateGoogle(req): q.IPromise<any>  {
        //https://developers.google.com/identity/sign-in/web/backend-auth
        var deferred = q.defer();

        let token = req.body.token;
        this.validateGoogleUserToken(token)
            .then((g_user: any) => {
                User.findOne({
                    'google.email': g_user.email
                }, (err, user: IUser) => {
                    if (err) throw err;
                    if (!user) {

                        let newUser = <IUser>{
                            name: g_user.name
                        };
                        newUser.google = {
                            id: g_user.sub,
                            email: g_user.email,
                            token: token,
                            firstName: g_user.given_name,
                            familyName: g_user.family_name
                        };

                        user = new User(newUser);
                    }
                    
                    user.save((err, saved) => {
                        if (err) throw err;

                        //Pega token e manda pro front.
                        let token : string = jwt.sign({ id: saved.id }, config.jsonWebTokenSecret, { expiresIn: '365d' });
                        
                        let result = {
                            token: token,
                            userId: saved.id
                        };

                        deferred.resolve(result);
                    });
                });
            }).catch((error) => {
                throw error;
            });
        return deferred.promise;
    }

    private validateGoogleUserToken(token: string) {
        return new Promise((resolve, reject) => {
            https.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token, (res) => {
                res.on('data', (d) => {
                    var data = JSON.parse(<any>d);

                    // If the token is properly signed and the iss and exp claims have the expected values, you will get a HTTP 200 response
                    if (res.statusCode == 200
                        //you still need to check that the aud claim contains one of your app's client IDs
                        && data.aud == config.google.clientId) {
                        resolve(data);
                    } else {
                        reject(data.error || data);
                    }

                });
            }).on('error', (e) => {
                reject(false);
            });
        });
    }

}

export const authenticationService = new AuthenticationService()