export interface Config {
//    root: string,
    app: {
        name: string
    },
    google: {
        clientId: string
    },
    port: number,
    jsonWebTokenSecret: string,
    db: string
}

var configurations = {
    development: {
  //      root: rootPath,
        app: {
            name: 'car-parking-search'
        },
        google: {
            clientId: '396039287130-if9n5kakp0lnag2chu1jmkvrrvoo5le5.apps.googleusercontent.com'
        },
        port: 8080,
        jsonWebTokenSecret: 'osulehomeupaiseoriograndeomeular',
        db: 'localhost:27017/car-parking-search'
    }
}

export const config: Config = configurations['development'];
