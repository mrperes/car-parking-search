import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { RouterModule, Routes }   from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app/app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NotifyComponent } from './notify/notify.component';
import { ProfileComponent } from './profile/profile.component';

import { NotifyService } from './notify/notify.service';
import { ProfileService } from './profile/profile.service';
import { HttpClient } from './HttpClient';

const carRoutes: Routes = [
    {
      path: 'home',
      component: HomeComponent,
      children: [
        {
          path: 'notify',
          component: NotifyComponent,
          outlet: 'abc'
        },
        {
          path: 'profile',
          component: ProfileComponent,
          outlet: 'abc'
        },
      ]
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    } /* ,
    { path: '**', component: PageNotFoundComponent } */
];

@NgModule({
  imports:  [
    BrowserModule,
    HttpModule,
    MaterialModule,
    FormsModule,
    RouterModule.forRoot(carRoutes)
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NotifyComponent,
    ProfileComponent
  ],
  providers: [
    NotifyService,
    ProfileService,
    HttpClient
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
