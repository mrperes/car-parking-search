import { Component, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from './../HttpClient';

@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  name = 'Car parking search';

  constructor(private httpClient: HttpClient,
    private router: Router,
    private zone: NgZone) {

  }

  ngOnInit() {
    //this.router.navigate(['home']);
  }

  // ngAfterViewInit() {
  //   setTimeout(() => { this.router.navigate(['home']) }, 1000);
  // }

  fShowInfo = false;
  ngAfterViewInit() {
    gapi.signin2.render('my-signin2', {
      'onsuccess': (param: any) => { this.onSignIn(param); },
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'light'

    });
  }
  onSignIn(googleUser: any) {
    this.fShowInfo = true;
    var profile = googleUser.getBasicProfile();
    // console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    // console.log('Full Name: ' + profile.getName());
    // console.log('Given Name: ' + profile.getGivenName());
    // console.log('Family Name: ' + profile.getFamilyName());
    // console.log("Image URL: " + profile.getImageUrl());
    // console.log("Email: " + profile.getEmail());

    let token = googleUser.getAuthResponse().id_token;
    let data = { token: token, loginApi: 'google' };
    this.httpClient.post('login', data).subscribe(p => {
      localStorage.setItem('carsToken', p.access_token);
      localStorage.setItem('userId', p.userId);

      this.zone.run(() => {
        this.router.navigate(['home']);
      })
    });
  };

  signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut();
    this.fShowInfo = false;
  }

}
