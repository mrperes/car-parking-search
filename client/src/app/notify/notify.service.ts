import { Injectable } from '@angular/core';
import { HttpClient } from './../HttpClient';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Push } from './push';

@Injectable()
export class NotifyService {

  constructor(private httpClient: HttpClient) { }

  getLastPushes(): Observable<Push[]> {

    // var mock: Push[] = [
    //   { plaque: 'teste1', date: 'agora a' },
    //   { plaque: 'teste2', date: 'agora a' },
    //   { plaque: 'teste3', date: 'agora a' },
    //   { plaque: 'teste4', date: 'agora a' },
    //   { plaque: 'teste5', date: 'agora a' },
    //   { plaque: 'teste6', date: 'agora a' }
    // ];

    // return Observable.of(mock);

    return this.httpClient.get('getAllSent');
  }

  notifyBlocking(plaque: string) {
    return this.httpClient.post('notifyBlocking', { 'plaque': plaque })
      .subscribe(p => console.log(p));
  }

  notifyLight(plaque: string) {
    this.httpClient.post('notifyLight', { plaque: plaque })
      .subscribe(p => console.log(p));
  }
}
