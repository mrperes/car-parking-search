import { Component, OnInit } from '@angular/core';

import { NotifyService } from './notify.service';
import { Push } from './push';

@Component({
  moduleId: module.id,
  selector: 'notify',
  templateUrl: './notify.component.html'
})
export class NotifyComponent implements OnInit  {

  private lastPushes : Push[];
  plaque: string;

  constructor(
    private notifyService:NotifyService) { }

  ngOnInit(){
    this.getLastPushes();
  }

  getLastPushes() {
    this.notifyService
    .getLastPushes()
    .subscribe((result) => {
      debugger;
      this.lastPushes = result;
    });
  }

  notifyBlocking() {
    this.notifyService.notifyBlocking(this.plaque);
  }

  notifyLight() {
    this.notifyService.notifyLight(this.plaque);
  }
}
