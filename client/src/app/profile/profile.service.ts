import { Injectable } from '@angular/core';
import { HttpClient } from './../HttpClient';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class ProfileService {

  constructor(private httpClient: HttpClient) { }

  getUserData(): Observable<User> {
    return this.httpClient.get('getUserInfo');
  }

  // redirect() {
  //   return this.httpClient.get('redirect');
  // }

  save(user: User): Observable<User>{
    return this.httpClient.post('saveUser', user);
  }
}
