import { Component, OnInit, NgZone, ChangeDetectionStrategy } from '@angular/core';

import { ProfileService } from './profile.service';

import { User } from './user';

@Component({
  moduleId: module.id,
  selector: 'profile',
  templateUrl: './profile.component.html',
  //changeDetection: ChangeDetectionStrategy.Default
})
export class ProfileComponent implements OnInit {

  user: User;
  plaques: string[];
  plaqueToAdd: string;

  constructor(
    private profileService: ProfileService,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.profileService.getUserData()
      .subscribe((userAbc) => {
        this.user = userAbc;
      }, err => console.log(err));
  }

  addPlaque() {
    this.user.plaques.push(this.plaqueToAdd);
    this.plaqueToAdd = "";
  }

  removePlaque(plaque: string) {
    this.user.plaques.splice(this.user.plaques.indexOf(plaque), 1);
  }

  // redirect() {
  //   this.profileService.redirect().subscribe(p => console.log(p));
  // }

  save() {
    this.profileService.save(this.user)
      .subscribe((savedUser: User) => {
        this.user = savedUser;
      });
  }

  cancel() {
    this.getUser();
  }

}
