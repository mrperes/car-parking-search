import { Component, OnInit } from '@angular/core';
import { HttpClient } from './../HttpClient';

@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    if (!('Notification' in window)) {
      alert('Desktop notifications not available in your browser. Try Chromium.');
      return;
    }

    if ((<any>window).Notification.permission !== 'granted') {
      let me = this;
      (<any>window).Notification.requestPermission().then((permission: string) => {
        if (permission === 'granted') {
          if ('serviceWorker' in navigator) {
            (<any>navigator).serviceWorker.register('/serviceworker.js'/*, {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': 'http://localhost:3000'
            }*/)
              .then((p: any) => { this.initialiseState() })
              .catch((err: any) => console.log(err));
          } else {
            console.log('service worker not present');
          }
        }
      });
    }

    if ((<any>window).Notification.permission === 'granted') {
      (<any>navigator).serviceWorker.ready.then((registration: any) => {
        registration.pushManager.getSubscription().then((subscription: any) => {
          if (subscription) {
            this.getToken(subscription);
          }
        });
      });
    }
  }

  initialiseState() {
    debugger;
    //check if user has blocked push notification
    if ((<any>window).Notification.permission === 'denied') {
      console.warn('User has blocked the notification');
    }
    //check if push messaging is supported or not
    if (!('PushManager' in window)) {
      console.warn('Push messaging is not supported');
      return;
    }

    //subscribe to GCM
    (<any>navigator).serviceWorker.ready.then((serviceWorkerRegistration: any) => {
      //call subscribe method on serviceWorkerRegistration object
      serviceWorkerRegistration.pushManager.subscribe({ userVisibleOnly: true })
        .then((subscription: any) => {
          this.getToken(subscription);
          var met = JSON.stringify(subscription);
          console.log('Mensagem', met);
        }).catch((err: any) => {
          console.error('Error occured while subscribe(): ', err);
        });
    });
  }

  getToken(subscription: any) {
    console.log(subscription);
    let token = subscription.endpoint.substring(40, subscription.endpoint.length);
    let data = { deviceId: token };
    //Se ja estiver autenticado
    this.httpClient.post('register', data)
      .subscribe(
      p => console.log('register => ' + p),
      err => console.log('register => ' + err));
    try {
      console.log(token);
    } catch (e) {

    }
  }

}

