import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpClient {

  private backendServer: string = 'http://jstein:8080/';
  private clientServer: string = 'http://jstein:3000';
  private api: string = 'api/';

  constructor(private http: Http,
    private router: Router) { }

  createAuthorizationHeader(headers: Headers) {
    let accessToken = localStorage.getItem('carsToken');
    let userId = localStorage.getItem('userId');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', this.clientServer);
    headers.append('x-access-token', accessToken);
    headers.append('user-id', userId);
  }

  private urlValidate(url: string) {
    if (url.startsWith('/')) {
      return url.substring(1);
    }
    return url;
  }

  private handleError(error: any): Observable<any> {
    if (error.status == 403) {
      this.router.navigate(['/login']);      
    }
    //console.error('An error occurred', error); // for demo purposes only
    return error.message || error;
  }

  private handleRequest(response: any): any {
    if (response.statusCode >= 300 && response.statusCode < 404) {
      this.router.navigate(['/login']);
    }
    else {
      return response
    }
  }

  get(url: string): Observable<any> {
    url = this.urlValidate(url);
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http
      .get(this.backendServer + this.api + url, {
        headers: headers
      })
      .map(response => response.json())
      .map((response) => { return this.handleRequest(response); })
      .catch((err: any) => { return this.handleError(err); });
  }

  post(url: string, data: any): Observable<any> {
    url = this.urlValidate(url);
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http
      .post(this.backendServer + this.api + url, data, {
        headers: headers
      })
      .map(response => response.json())
      .map((response) => { return this.handleRequest(response); })
      .catch((err: any) => { return this.handleError(err); });
  }


}
